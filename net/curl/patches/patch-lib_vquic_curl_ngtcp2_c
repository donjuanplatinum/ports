Revert "ngtcp2: use 0.15.0"
https://github.com/curl/curl/commit/10998e3b89be65e2bf9d9666bd9a1734c186ec0a

Revert "ngtcp2: build with 0.16.0 and nghttp3 0.12.0"
https://github.com/curl/curl/commit/cae9d10be3d3446beda9e217a0abca91b1b2ab79

Revert "ngtcp2: build with 0.17.0 and nghttp3 0.13.0"
https://github.com/curl/curl/commit/e0093b4b732f6495b0fb1cd6747cbfedcdcf63ed

Index: lib/vquic/curl_ngtcp2.c
--- lib/vquic/curl_ngtcp2.c.orig
+++ lib/vquic/curl_ngtcp2.c
@@ -33,7 +33,7 @@
 #ifdef OPENSSL_IS_BORINGSSL
 #include <ngtcp2/ngtcp2_crypto_boringssl.h>
 #else
-#include <ngtcp2/ngtcp2_crypto_quictls.h>
+#include <ngtcp2/ngtcp2_crypto_openssl.h>
 #endif
 #include "vtls/openssl.h"
 #elif defined(USE_GNUTLS)
@@ -140,7 +140,7 @@ struct cf_ngtcp2_ctx {
   uint32_t version;
   ngtcp2_settings settings;
   ngtcp2_transport_params transport_params;
-  ngtcp2_ccerr last_error;
+  ngtcp2_connection_close_error last_error;
   ngtcp2_crypto_conn_ref conn_ref;
 #ifdef USE_OPENSSL
   SSL_CTX *sslctx;
@@ -351,7 +351,7 @@ static void quic_settings(struct cf_ngtcp2_ctx *ctx,
   t->initial_max_streams_uni = QUIC_MAX_STREAMS;
   t->max_idle_timeout = QUIC_IDLE_TIMEOUT;
   if(ctx->qlogfd != -1) {
-    s->qlog_write = qlog_callback;
+    s->qlog.write = qlog_callback;
   }
 }
 
@@ -407,8 +407,8 @@ static CURLcode quic_ssl_ctx(SSL_CTX **pssl_ctx,
     goto out;
   }
 #else
-  if(ngtcp2_crypto_quictls_configure_client_context(ssl_ctx) != 0) {
-    failf(data, "ngtcp2_crypto_quictls_configure_client_context failed");
+  if(ngtcp2_crypto_openssl_configure_client_context(ssl_ctx) != 0) {
+    failf(data, "ngtcp2_crypto_openssl_configure_client_context failed");
     goto out;
   }
 #endif
@@ -754,9 +754,9 @@ static int cb_recv_stream_data(ngtcp2_conn *tconn, uin
   DEBUGF(LOG_CF(data, cf, "[h3sid=%" PRId64 "] read_stream(len=%zu) -> %zd",
                 stream_id, buflen, nconsumed));
   if(nconsumed < 0) {
-    ngtcp2_ccerr_set_application_error(
-      &ctx->last_error,
-      nghttp3_err_infer_quic_app_error_code((int)nconsumed), NULL, 0);
+    ngtcp2_connection_close_error_set_application_error(
+        &ctx->last_error,
+        nghttp3_err_infer_quic_app_error_code((int)nconsumed), NULL, 0);
     return NGTCP2_ERR_CALLBACK_FAILURE;
   }
 
@@ -813,8 +813,8 @@ static int cb_stream_close(ngtcp2_conn *tconn, uint32_
   DEBUGF(LOG_CF(data, cf, "[h3sid=%" PRId64 "] quic close(err=%"
                 PRIu64 ") -> %d", stream3_id, app_error_code, rv));
   if(rv) {
-    ngtcp2_ccerr_set_application_error(
-      &ctx->last_error, nghttp3_err_infer_quic_app_error_code(rv), NULL, 0);
+    ngtcp2_connection_close_error_set_application_error(
+        &ctx->last_error, nghttp3_err_infer_quic_app_error_code(rv), NULL, 0);
     return NGTCP2_ERR_CALLBACK_FAILURE;
   }
 
@@ -926,13 +926,13 @@ static int cb_get_new_connection_id(ngtcp2_conn *tconn
   return 0;
 }
 
-static int cb_recv_rx_key(ngtcp2_conn *tconn, ngtcp2_encryption_level level,
+static int cb_recv_rx_key(ngtcp2_conn *tconn, ngtcp2_crypto_level level,
                           void *user_data)
 {
   struct Curl_cfilter *cf = user_data;
   (void)tconn;
 
-  if(level != NGTCP2_ENCRYPTION_LEVEL_1RTT) {
+  if(level != NGTCP2_CRYPTO_LEVEL_APPLICATION) {
     return 0;
   }
 
@@ -1017,7 +1017,7 @@ static CURLcode check_and_set_expiry(struct Curl_cfilt
       if(rv) {
         failf(data, "ngtcp2_conn_handle_expiry returned error: %s",
               ngtcp2_strerror(rv));
-        ngtcp2_ccerr_set_liberr(&ctx->last_error, rv, NULL, 0);
+        ngtcp2_connection_close_error_set_transport_error_liberr(&ctx->last_error, rv, NULL, 0);
         return CURLE_SEND_ERROR;
       }
       result = cf_progress_ingress(cf, data, pktx);
@@ -1286,8 +1286,7 @@ static int cb_h3_stop_sending(nghttp3_conn *conn, int6
   (void)conn;
   (void)stream_user_data;
 
-  rv = ngtcp2_conn_shutdown_stream_read(ctx->qconn, 0, stream_id,
-                                        app_error_code);
+  rv = ngtcp2_conn_shutdown_stream_read(ctx->qconn, stream_id, app_error_code);
   if(rv && rv != NGTCP2_ERR_STREAM_NOT_FOUND) {
     return NGTCP2_ERR_CALLBACK_FAILURE;
   }
@@ -1305,7 +1304,7 @@ static int cb_h3_reset_stream(nghttp3_conn *conn, int6
   (void)conn;
   (void)data;
 
-  rv = ngtcp2_conn_shutdown_stream_write(ctx->qconn, 0, stream_id,
+  rv = ngtcp2_conn_shutdown_stream_write(ctx->qconn, stream_id,
                                          app_error_code);
   DEBUGF(LOG_CF(data, cf, "[h3sid=%" PRId64 "] reset -> %d", stream_id, rv));
   if(rv && rv != NGTCP2_ERR_STREAM_NOT_FOUND) {
@@ -1329,8 +1328,7 @@ static nghttp3_callbacks ngh3_callbacks = {
   cb_h3_stop_sending,
   NULL, /* end_stream */
   cb_h3_reset_stream,
-  NULL, /* shutdown */
-  NULL /* recv_settings */
+  NULL /* shutdown */
 };
 
 static int init_ngh3_conn(struct Curl_cfilter *cf)
@@ -1340,7 +1338,7 @@ static int init_ngh3_conn(struct Curl_cfilter *cf)
   int rc;
   int64_t ctrl_stream_id, qpack_enc_stream_id, qpack_dec_stream_id;
 
-  if(ngtcp2_conn_get_streams_uni_left(ctx->qconn) < 3) {
+  if(ngtcp2_conn_get_max_local_streams_uni(ctx->qconn) < 3) {
     return CURLE_QUIC_CONNECT_ERROR;
   }
 
@@ -1915,12 +1913,13 @@ static CURLcode recv_pkt(const unsigned char *pkt, siz
                   ngtcp2_strerror(rv)));
     if(!ctx->last_error.error_code) {
       if(rv == NGTCP2_ERR_CRYPTO) {
-        ngtcp2_ccerr_set_tls_alert(&ctx->last_error,
-                                   ngtcp2_conn_get_tls_alert(ctx->qconn),
-                                   NULL, 0);
+        ngtcp2_connection_close_error_set_transport_error_tls_alert(
+            &ctx->last_error,
+            ngtcp2_conn_get_tls_alert(ctx->qconn), NULL, 0);
       }
       else {
-        ngtcp2_ccerr_set_liberr(&ctx->last_error, rv, NULL, 0);
+        ngtcp2_connection_close_error_set_transport_error_liberr(
+            &ctx->last_error, rv, NULL, 0);
       }
     }
 
@@ -2006,9 +2005,9 @@ static ssize_t read_pkt_to_send(void *userp,
       if(veccnt < 0) {
         failf(x->data, "nghttp3_conn_writev_stream returned error: %s",
               nghttp3_strerror((int)veccnt));
-        ngtcp2_ccerr_set_application_error(
-          &ctx->last_error,
-          nghttp3_err_infer_quic_app_error_code((int)veccnt), NULL, 0);
+        ngtcp2_connection_close_error_set_application_error(
+            &ctx->last_error,
+            nghttp3_err_infer_quic_app_error_code((int)veccnt), NULL, 0);
         *err = CURLE_SEND_ERROR;
         return -1;
       }
@@ -2048,7 +2047,8 @@ static ssize_t read_pkt_to_send(void *userp,
         DEBUGASSERT(ndatalen == -1);
         failf(x->data, "ngtcp2_conn_writev_stream returned error: %s",
               ngtcp2_strerror((int)n));
-        ngtcp2_ccerr_set_liberr(&ctx->last_error, (int)n, NULL, 0);
+        ngtcp2_connection_close_error_set_transport_error_liberr(
+            &ctx->last_error, (int)n, NULL, 0);
         *err = CURLE_SEND_ERROR;
         nwritten = -1;
         goto out;
@@ -2420,7 +2420,7 @@ static CURLcode cf_connect_start(struct Curl_cfilter *
   ngtcp2_conn_set_tls_native_handle(ctx->qconn, ctx->ssl);
 #endif
 
-  ngtcp2_ccerr_default(&ctx->last_error);
+  ngtcp2_connection_close_error_default(&ctx->last_error);
 
   ctx->conn_ref.get_conn = get_conn;
   ctx->conn_ref.user_data = cf;
@@ -2496,7 +2496,7 @@ static CURLcode cf_ngtcp2_connect(struct Curl_cfilter 
 
 out:
   if(result == CURLE_RECV_ERROR && ctx->qconn &&
-     ngtcp2_conn_in_draining_period(ctx->qconn)) {
+     ngtcp2_conn_is_in_draining_period(ctx->qconn)) {
     /* When a QUIC server instance is shutting down, it may send us a
      * CONNECTION_CLOSE right away. Our connection then enters the DRAINING
      * state.
